# Essempi per la programmazione PHP OOP not Procedural!

A partire dalla versione 5, PHP, dopo essere stato solo un linguaggio procedurale, si è affacciato alla programmazione ad oggetti, introducendo le classi.

E' una significativa evoluzione di PHP, ed oggi possiamo tranquillamente paragonarlo ad altri più affermati linguaggi che lavorano ad oggetti come ad esempio JAVA, anche se PHP è per sua natura solo "web oriented" e quindi presenta limiti sia nell’architettura che nella sintassi. Per questo motivo spesso si parla di "programmazione in stile OOP" e non proprio di "Programmazione Orientata agli Oggetti".

Alla base di questa "Programmazione Orientata agli Oggetti" vi sono le classi.

## Struttura di una classe

Per definire una classe nel nostro script PHP utilizziamo la parola chiave "class" seguita da un nome a nostra scelta, e non deve contenere spazi.
Per convenzione si utilizza la prima lettera maiuscola, e nel caso questo nome contenga più parole, in maiuscolo la prima lettera di ogni parola.

```
class MioTest
{
    #proprietà, cioè una variabile
    public $nome="pippo";

    #metodo, cioè una funzione
    public function saluto(){
       echo "ciao da ".$this->nome;
    }
}
```


#istanza della classe:
`$obj= new MioTest;`
#accedo alla funzione "saluto" utilizzando una freccia seguita dal nome della funzione
`$obj -> saluto();`

All'interno nel metodo "saluto" abbiamo utilizzato la proprietò "nome" utilizzando una freccia
Il comando $this identifica, tramite l’operatore ->, una proprietà all’interno di questa classe.
`$this->nome`;
in modo simile a "$obj -> saluto", ma in questo caso siamo dentro la classe e qui dentro si utilzza "$this".


## Visibilità di metodi e proprietà

Abbiamo visto negli esempi precedenti quel "**public**" anteposto al nome del metodo e della proprietà.
Questa è la visibilità del metodo e della proprietà, e descrive la possibilità di accedere, a questo metodo o proprietà, dall'esterno della classe, e dall'inter
**public**: significa che sono accessibili anche da fuori della classe
**protected**: accessibili sono dall'interno della classe
**private**: accessibili sono dall'interno della classe, ma non nelle classi CHILD

### Visilbilità PUBLIC di metodi e proprietà
Il metodo "saluto" ha visibilità public, per cui posso accedere al metodo dall'esterno della classe, ed è quello che abbiamo fatto quando, dopo aver creare l'oggetto di classe "MioTest" abbiamo chiamato il metodo "saluto" così
`$obj -> saluto();`
Se quel metodo fosse stato "private" o "protected", non avremmo potuto farlo. A titolo di esempio prova a impostare come "private" quel metodo, e rilancia lo script. Il risultato sarà il seguente:
_Fatal error: Uncaught Error: Call to private method MioTest::saluto() ...._

Se la proprietà ha visibilità pubblica, posso utilizzarla dentro e fuori la classe. Dentro, abbiamo già visto come. Fuori lo vediamo adesso e lo facciamo in modo analogo a quando fatto per accedere al metodo, con la freccia:

```
class MioTest
{
    // proprietà, cioè una variabile
    public $nome="pippo";

    // metodo, cioè una funzione
    public function saluto(){
       echo "ciao da ".$this->nome;
    }
}

// creo un oggetto di classe "MioTest"
$obj= new MioTest;
// accedo alla funzione "saluto"
$obj -> saluto();
echo "<br>"
// accedo alla proprietà pubblica $nome
$obj -> nome;
```
**L'ouput sarà**
> ciao da pippo
> pippo

Non solo posso utilizzarlo da fuori, ma possiamo anche modificarlo, assegnandogli un valore diverso, in questo modo

```
class MioTest
{
    // proprietà, cioè una variabile
    public $nome="pippo";

    // metodo, cioè una funzione
    public function saluto(){
       echo "ciao da ".$this->nome;
    }
}

// creo un oggetto di classe "MioTest"
$obj= new MioTest;
// assegno alla proprietà "nome" il valore "pluto"
$obj -> nome="pluto";
// accedo alla funzione "saluto"
$obj -> saluto();
```
**L'output sarà**

> ciao da pluto

### Visilbilità PRIVATE di metodi e proprietà

Come abbiamo già detto, una visibilità private consente di usare metodi e proprietà solo all'interno della classe.

Modifichiamo il nostro esempio così, assegnando visibilità private alla proprietà "nome". Possiamo tranquillamente continuare ad utilizzarlo nella classe, ma non da fuori.

```
class MioTest
{
    // proprietà, cioè una variabile
    private $nome="pippo";

    // metodo, cioè una funzione
    public function saluto(){
       echo "ciao da ".$this->nome;
    }
}

// creo un oggetto di classe "MioTest"
$obj= new MioTest;
// accedo alla funzione "saluto"
$obj -> saluto();
```

### Ereditarietà delle classi
Visilbilità PRIVATE di metodi e proprietà -> Simili ai precedenti, MA non sono accessibili dalle classi "child", ma per spiegare cosa sono le classi "child" dobbiamo introdurre il concetto di ereditarietà delle classi, e lo facciamo subito 

Una classe detta "figlia" o "child" può ereditare proprietà e metodi di un'altra classe "padre" o "parent".

Capiamo subito questo concetto con un esempio. Abbiamo una classe A con dentro tre prorietà (public, private e protected) e tre metodi (public, private e protected) così possiamo vedere in azione tutte le possibilità visibilità

```
class A {
    // proprietà
    public $isPropPub=1;
    private $isPropPriv=2;
    protected $isPropProt=3;

    // metodi
    public function isMetPub(){
        echo "Sto usando il metodo isMetPub della classe A<br />";
    }
    public function isMetPriv(){
        echo "Sto usando il metodo isMetPriv della classe A<br />";
    }
    public function isMetProt(){
        echo "Sto usando il metodo isMetProt della classe A<br />";
    }
}

class B extends A {
	public function test() {
	       echo "Sto usando la classe B, il valore della proprieta isPropPub è ".$this->isPropPub;
	}
}

// istanzio un oggetto di classe A
$obj = new A;
// accedo al metodo pubblico della classe A "isMetPub"
$obj -> isMetPub();

// istanzio un oggetto di classe B
$obj = new B;
// accedo al metodo test della classe B "test"
$obj -> test();
```
**Il risultato sarà questo**

> Sto usando il metodo isMetPub della classe A
> Sto usando la classe B, il valore della proprieta isPropPub è 1

**Esiste un limite all'ereditarietà: PHP non supporta l'ereditarietà multilpla, cioè una classe non può ereditare più classi**
**Ma è possibile superare questo limite con i TRAIT.**

### Override dei metodi e final OOP



